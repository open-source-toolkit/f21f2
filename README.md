# 基于STM32+RN8302B的电压采集项目

## 项目描述

本项目利用STM32F103RET6微控制器和RN8302B电能计量芯片，实现对C相电压的采集。通过此项目，您可以熟悉RN8302B芯片的操作，并掌握STM32CubeIDE和STM32CubeMX的使用。

## 适用人群

本项目适合具备一定编程基础，工作1-3年的研发人员。

## 学习目标

通过本项目，您将学习到以下内容：

1. **STM32CubeIDE的使用**：了解如何使用STM32CubeIDE进行开发。
2. **STM32CubeMX配置**：掌握如何使用STM32CubeMX对STM32F103RCT6芯片进行引脚配置。
3. **printf重映射**：学习如何在STM32上实现`printf`函数的重映射。
4. **RN8302B操作**：掌握如何操作RN8302B芯片进行电压采集。

## 阅读建议

本项目通过开发一个简单的交流电压采集功能，帮助您熟悉RN8302B的操作。其优点包括：

- 基于ST官方、正版、免费的STM32CubeIDE开发。
- 内置的STM32CubeMX工具使得引脚配置非常直观，移植非常方便，只需确保控制引脚命名相同即可。

## 移植教程

如需移植本项目，请参考以下教程：

[移植教程链接](https://blog.csdn.net/qq_35629563/article/details/126772062)

## 资源文件

本仓库提供了实现上述功能的资源文件，包括源代码、配置文件等。请根据需要下载并使用。

## 贡献

欢迎大家提出改进建议或提交PR，共同完善本项目。

## 许可证

本项目采用开源许可证，具体信息请参阅LICENSE文件。